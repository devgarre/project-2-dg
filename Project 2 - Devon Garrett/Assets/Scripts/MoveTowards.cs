﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour {

	private Transform tf;
	private Transform targetTf;
	private Vector3 movementVector;
	public float speed;
	public bool isAlwaysSeeking;

	// Use this for initialization
	void Start () {
		tf = GetComponent <Transform> ();
		targetTf = GameObject.Find("tank_huge").GetComponent<Transform>();
		movementVector = targetTf.position - tf.position;
		tf.up = movementVector * -1;
	}
	
	// Update is called once per frame
	void Update () {
		if (isAlwaysSeeking) 
		{
			movementVector = targetTf.position - tf.position;
			tf.up = movementVector;
		}
		movementVector.Normalize ();
		movementVector = movementVector * speed;
		tf.position = tf.position + movementVector;
	}
}
