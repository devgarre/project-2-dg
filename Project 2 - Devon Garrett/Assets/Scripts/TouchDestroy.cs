﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDestroy : MonoBehaviour {

	private GameObject[] currentEnemies;
	private int i;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Player") 
		{
			currentEnemies = GameObject.FindGameObjectsWithTag ("Enemy");
			for (i = 0; i < currentEnemies.Length; i++) {
				Destroy (currentEnemies [i]);
			}
		}
		if (other.tag == "Bullet") 
		{
			Destroy (other.gameObject);
			Destroy (gameObject);
		}
	}
}
