﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

	public int playerSpeed;
	public int backSpeed;
	public int rotateSpeed;
	public int playerLives;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("left")) 
		{
			transform.Rotate (new Vector3 (0, 0, rotateSpeed));
		}
		if (Input.GetKey ("right")) 
		{
			transform.Rotate (new Vector3 (0, 0, rotateSpeed * -1));
		}
		if (Input.GetKey ("up")) 
		{
			transform.position -= transform.up * playerSpeed * Time.deltaTime;
		}
        if (Input.GetKey("down"))
        {
            transform.position += transform.up * backSpeed * Time.deltaTime;
        }
	}

	void OnBecameInvisible() {
		Destroy (gameObject);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Enemy") 
		{
			transform.position = new Vector3 (0, 0, 0);
			playerLives = playerLives - 1;
			if (playerLives <= 0) 
			{
				Application.Quit ();
			}

		}
	}
}
