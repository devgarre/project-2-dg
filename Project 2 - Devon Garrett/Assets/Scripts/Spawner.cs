﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public GameObject[] enemies;
	public Vector3 spawnLocation;
	public float waitTime;
	public float spawnLongestTime;
	public float spawnLeastTime;
	public int startWait;
	public int verticalHorizontal;
	public float spawnX;
	public float spawnY;
	public static int enemyCount = 0;
	public bool spawnEnemies = true;

	int randEnemy;

	// Use this for initialization
	void Start () {
		StartCoroutine (spawnerWait());
	}
	
	// Update is called once per frame
	void Update () {
		waitTime = Random.Range (spawnLeastTime, spawnLongestTime);
	}

	IEnumerator spawnerWait (){
		yield return new WaitForSeconds(startWait);
		
		while (enemyCount <= 2) {
			randEnemy = Random.Range (0, 5);
			verticalHorizontal = Random.Range (0, 2);
			if (verticalHorizontal == 0) {
				spawnX = Random.Range (0, 1);
				spawnY = Random.Range (0.0f, 1.1f);
			} else {
				spawnX = Random.Range (0.0f, 1.1f);
				spawnY = Random.Range (0, 1);
			}

			Vector3 spawnLocation = new Vector3 (spawnX, spawnY, 10);

			Instantiate (enemies [randEnemy], Camera.main.ViewportToWorldPoint (spawnLocation), gameObject.transform.rotation);

			yield return new WaitForSeconds (waitTime);

			enemyCount = enemyCount + 1;
			while (enemyCount > 2) {
				yield return new WaitForSeconds (waitTime);
			}
		}
	}
}
